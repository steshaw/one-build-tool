# One tool to build them all

This page discusses some of the more interesting attempts at solving the build
problem, and where I think they fall down.

## Clearcase

In a past life I had the misfortune to be stuck working with
[Clearcase](http://en.wikipedia.org/wiki/IBM_Rational_ClearCase).
I hated it so much that I even
[wrote my own bridge to Git](https://github.com/charleso/git-cc).

Despite it's many flaws it also has some interesting features that we
currently lack in this new Git world.

A large part of Clearcase is the concept of "dynamic views", where a custom
filesystem is used to provide a "view" of the entire code base, but files are
only downloaded on demand.

This can be combined with
[Clearmake](http://publib.boulder.ibm.com/infocenter/cchelp/v7r0m0/topic/com.ibm.rational.clearcase.cc_ref.doc/topics/clearmake.htm),
which caches binary artifacts derived from input source files, which can be 
winked in" to another view with those identical inputs.

A few quick observations.

1. The problem with Clearcase is that it does everything per-file. SVN and Git
have shown us the power of atomic commits, but I think the wheel is
turning and we need to cache build results based on "modules", rather than
"files". But it's the same shit, just at a different level.

2. Custom file systems make clever tricks like Clearmake very easy, you
can automatically detect every input without needing explicit declaration.
However custom filesystems are complex, and I suspect the future lies elsewhere.

This actually sounds a lot like
[Google's file system](http://google-engtools.blogspot.com.au/2011/06/build-in-cloud-accessing-source-code.html)

## Look Ma, No Pants

Two of the most interesting build systems that are emerging at the moment are
[Buck](http://facebook.github.io/buck) and [Pants](http://pantsbuild.github.io/),
both based on
[Google's Blaze](http://google-engtools.blogspot.com.au/2011/08/build-in-cloud-how-build-system-works.html).

However, I would argue that both have a fundamental flaw that I suspect will
make them impossible for handling modules at different versions.
And that is that they assume everything is kept in a single folder structure.
A temporary work around would be to use Git Submodules. But consider Buck build
target's, which are always absolute. What happens if you have:

    Project A:
        - BUILD
        - /lib/foo
        - Project B (Git submodule)
            - BUILD
                - //lib/foo
            - /lib/foo

At least at the moment `//lib/foo` will mean something different depending on
where Buck decides is the "root".

Buck and Pants are just not designed for building sub-components of a system.
How would you extract a submodule with it's own release cycle (eg. open 
sourcing a library)? The answer, I suspect, is you have to step outside the warm
comfort of your specialised build tool.

## Facebook

Facebook recently announced how they were using Mercurial to
[scale their codebase](https://code.facebook.com/posts/218678814984400/scaling-mercurial-at-facebook/
).

Some of the advantages of this approach.

1. Refactoring is easy - everything is technically in the same codebase
2. Nothing to "release"

Not surprisingly I agree with [the critics](https://news.ycombinator.com/item?id=7020143)
of this approach that they should be looking to modularise rather than lump
everything together.

## Cheating

As we've seen companies like Facebook and Google can solve their build
problems by "cheating".
And by cheating I mean they are making a reasonable optimization based on 
reducing variables, such as restricting everything to a single codebase.
To be clear I'm not criticising this approach, but I don't believe that is
reusable/useful in the general case, which is all I really care about.

To take an example of why this wouldn't work, let me talk about Atlassian.
We have a number of different products such as JIRA, Confluence and Stash.
Obviously we want to share common libraries, but each product has a different
release cycle. In a single codebase both would have to _always_ build against
the latest version of libraries (which isn't necessarily a bad thing, but that's
a topic for another day). Introducing a new feature in the library for one
product may break another accidentally. As hinted above I believe this is the
same problem faced at Google/Facebook if they decide to exact a library for
open sourcing.

So, what might my [ultimate build tool look like](02-ultimate-build-tool.md)?
