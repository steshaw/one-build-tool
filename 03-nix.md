# One tool to build them all

## Nix

[Nix](http://nixos.org/nix/) is a "purely functional package manager" that was
[conceived by Eelco Dolstra way back in 2003](https://nixos.org/docs/papers.html).
Nix captures _every_ input into a source build, which ensures they can be
recreated. In addition it supports downloading build artifacts to be used in
place of a source build if (and only if) the inputs are identical.

One of the things that caught my attention was Nix's companion build server
[Hydra](https://nixos.org/hydra/).
It can build a single Nix expression, but recognises the dependencies between 
builds which re-uses components. Here is an example of a build of common
dependency of a project (note the "cached from" link).

![hydra screenshot with cache](https://bytebucket.org/cofarrell/one-build-tool/raw/master/images/hydra1.png)

Hydra has the concept of a "release", which just promotes previous builds.

Finally, Nix has something called a "channel", which is a list of expressions
and their binary output. 
You can subscribe to a channel, and Nix will automatically
download binary artifacts if the local expression matches.
Hydra conveniently publishes a channel with the latest artifacts of each project.
Unreferenced artifacts can be garbage collected by running `nix-store --gc`.

This only touches on a few of features of Nix, but these are the ones that align
with my "wish list" of UBT features.

## Friends with benefits

An interesting benefit that falls out of Nix is that it understands _how_ to
build things. This is not necessary surprising if you think about source builds.

For example it's easy to think about have a Java-only build or \<insert
favourite  language here\>, but what happens if you introduce another language,
such as Javascript.
What happens if you want to compress the files before releasing or compile from
Coffeescript?
You _could_ write a specilised Maven plugin that has to be written in Java.
What happens if you want to make use of `node` or some non-java binary.
How do you ensure developers have their machine configured correctly?

To build with _any_ command in Nix, even simple commands like `mv` or `cp`, need
to be declared as inputs.
As part of the input to your build you must provide the recipe for these,
just like any of your other dependencies. When someone builds your script it
will first need to resolve those inputs, which may potentially result in a build
or download of `mv`, and so on. The same is true for `javac` or any other
compiler, why given them special treatment?

It's turtles all the way down.

![Turtles](http://www.nasuni.com/writable/images/Blog_Images/turtles-1-1-1.jpg)

### So what's the catch?

To use Nix as UBT comes with a few gotchas.

#### Recursive Builds

Nix currently doesn't support recursive builds. It requires that the entire
"expression" is on the local filesystem. This would require that _every_
dependency that is built with Nix for a given build would need to cloned,
regardless of whether you manage to download a binary artifact.
For someone just starting on a new project this could be incredibly cumbersome.

As of only a few months ago Shea Levy has
[opened a PR](https://github.com/NixOS/nix/pull/213)
that looks to rectify that.
I also had an enlightening
[discussion with Shea on IRC](http://nixos.org/irc/logs/log.20140427).

#### Full rebuilds

This one is a little trickier.

Let's take a very simple Java Nix project.

```
{ pkgs ? import <nixpkgs> {} }:

pkgs.java {
    dir = ./.;
    name = "helloworld";
}
```

And now it's corresponding "derivation".
This is what Nix resolves everything to, you can think of it like the "assembly
language" recipe of Nix.

```
Derive([("out","/nix/store/w0697grv83xykir3fmngwaly7mc5vmjh-helloworld","","")],[("/nix/store/6dmxczi276k08nzfzddhh3fk5wkkmwgb-bash-4.2-p45.drv",["out"]),("/nix/store/jmsakyghcbdqpv42a4rklazsnwv1asf4-openjdk6-b16-24_apr_2009-r1.drv",["out"]),("/nix/store/qvc02f9d05jgad3zg8d4w5dhbxyb6y75-stdenv.drv",["out"]),("/nix/store/yj6svnmlydgk04hysh6gkw0mvqf6isbs-bcprov-jdk15on-148.drv",["out"])],["/nix/store/9krlzvny65gdc8s7kpb6lkx8cd02c25b-default-builder.sh","/nix/store/r5s1hacbpdkgsppj8chgj2nrajgs0cd3-src"],"x86_64-darwin","/nix/store/85h1r887x7raf97l1invbm9zg1c1sfxr-bash-4.2-p45/bin/sh",["-e","/nix/store/9krlzvny65gdc8s7kpb6lkx8cd02c25b-default-builder.sh"],[("buildInputs",""),("buildPhase","javac -d . $(find $src -name \\*.java)\njar cfv $name.jar $(find . -name \\*.class)\n"),("builder","/nix/store/85h1r887x7raf97l1invbm9zg1c1sfxr-bash-4.2-p45/bin/sh"),("installPhase","mkdir -p $out/share/java\ncp $name.jar $out/share/java\n"),("name","helloworld"),("nativeBuildInputs","/nix/store/jfdm85gv2nymw7wryhfssw3hgpzaxnwg-openjdk6-b16-24_apr_2009-r1 /nix/store/k2c9pr2zymsa2zikk0ypsjd9risnyz4h-test /nix/store/i3bf5l4xabncljnx6gnmp3a430s2np44-bcprov-jdk15on-148"),("out","/nix/store/w0697grv83xykir3fmngwaly7mc5vmjh-helloworld"),("propagatedBuildInputs",""),("propagatedNativeBuildInputs",""),("src","/nix/store/r5s1hacbpdkgsppj8chgj2nrajgs0cd3-src"),("stdenv","/nix/store/kamf5pgp6fx5gmhk86csg8w43l1mcdln-stdenv"),("system","x86_64-darwin"),("userHook","")])
```

Because Nix is tracking _every_ input, you might be able to see that
for my build I would first need `bash-4.2-p45` and `openjdk6-b16`.
For this binary artifact to be shared (via a channel) to other users, they would
also need those _exact_ dependencies.
Even the slightest change in Nix environment would result in a cascading build
of every library.

At least coming from my cosy Java world this seems excessive. Compiling an
artifact with Java 6.x will produce an artifact that is compatible with any
subsequent Java version and any OS, give or take methods that have been
removed from the core library.

I suspect this is an inevitable trade-off between fully reproducible builds and
convenience/speed. Once you start messing with what it means to be a dependency
you end up with builds that aren't always reproducible because you made an
assumption that was true at the time, but latest proved to be false.

## Ixnay on the eamdray

I was vaguely hoping that my investigations into Nix would have a more immediate
and practical outcome.
It certainly solves the source build problem in a powerful and elegant way,
but perhaps a little _too_ well.
Is there a middle ground? Can I have my cake and eat it too?
