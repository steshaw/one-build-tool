# One tool to build them all

## Ultimate Build Tool

To help expand on what I'm envisaging, let's step through through a typical day
of the life of a developer using UBT.

08:30 -
Bob starts work by pulling from Project A to get the latest. He runs a build,
which doesn't take very long because the build machine ran last night and he
downloads only new artifacts. That's probably a good thing because his team is
using Scala and it's slow at compiling.

09:00 -
As Bob works on a new feature he decides he needs to update a common library
that his company have open sourced. He runs `ubt checkout libB` which clones the
source of `libB` into his project and he adds that to his IDE.
Running `ubt build` does nothing yet because the files in `libB` haven't changed
yet.

10:00 -
Bob has finished work on his feature and adding a new method to libB. He runs
`ubt checkout -b feature/X`, `ubt commit -m "Feature X"` and `ubt push` which
commits the changes to the two different repositories, pushes the change in
project A to the internal Git server, and libB to Github. He then creates a
review in both systems.

10:01 -
The build server, which is configured for Project A, starts building detects
that libB has been updated and clones/builds this module first, and then builds
Project A.

11:00 -
The changes for Project A are accepted and then merged even though the PR for
libA is still open. When the build runs again, nothing needs to happen because
the filesystem is identical to the previous build.

12:00 -
Bob's team decides they want to do a release, and click the "release" button in
the build server, which does nothing but add a tag to the latest commit in
Project A and marks the artifact to keep.

12:05 -
Alice has just pulled the latest and run a build, which also does nothing but
download the new artifacts for both Project A and libB as they have already been
built.

13:00 -
The artifact server runs it's daily GC, cleaning up the artifacts from the
previous day, but leaving any artifacts that have been marked as "release"
(including all the dependencies).

Please excuse the farcical example, but I hope it illustrates what I think we
should be striving for. For developers at Facebook/Google I suspect some of this
is largely a reality, but what about the rest of us?  

In particular I want to point out a few things that are missing from the
previously mentioned build tools.

- No need for a manual "release" when depending on another repository
- A "release" is just a tagged commit, no bullshit `mvn release` required
- While there are multiple Git repositories, it acts like a single filesystem,
  but the advantage being that dependencies can be versioned independently at
  the module level, declared only as commit hashes.

So when can you start using UBT?
[Sadly it's a little more complicated than that...](03-nix.md)
